-- <<------------------------------------------- Configuration ------------------------------------------->>


--Create constants
-- ->App name
FULL_NAME = "LazyHealer"
SHORT_NAME = "LH"

-- ->Slash commands
SLASH_LH1 = "/lh"
SLASH_LH2 = "/lazyhealer"
SLASH_LH2 = "/LazyHealer"
SLASH_LH2 = "/lazyHealer"

-- ->Application Arguments
local ARG_ENABLE = "enable"
local ARG_DISABLE = "disable"
local ARG_ADD = "addReliefPlayer"
local ARG_REMOVE = "removeReliefPlayer"
local ARG_SHOW = "showReliefPlayers"
local ARG_STATUS = "status"
local ARG_FEATURE_PARTY = "party"
local ARG_FEATURE_WHISPER = "whispers"
local ARG_FEATURE_ALL = "all"
local FEATURE_TABLE = {ARG_FEATURE_PARTY, ARG_FEATURE_WHISPER, ARG_FEATURE_ALL}
local ARG_CMD_ENABLE_FEATURE_PARTY = SLASH_LH1 .. " " .. ARG_ENABLE .. " " .. ARG_FEATURE_PARTY
local ARG_CMD_DISABLE_FEATURE_PARTY = SLASH_LH1 .. " " .. ARG_DISABLE .. " " .. ARG_FEATURE_PARTY
local ARG_CMD_ENABLE_FEATURE_RELIEF_HEALER = SLASH_LH1 .. " " .. ARG_ENABLE .. " " .. ARG_FEATURE_WHISPER
local ARG_CMD_DISABLE_FEATURE_RELIEF_HEALER = SLASH_LH1 .. " " .. ARG_DISABLE .. " " .. ARG_FEATURE_WHISPER

-- ->Messages
local LH_PARTY_ENABLED_MSG = FULL_NAME.." Party Chat Enabled"
local LH_PARTY_DISABLED_MSG = FULL_NAME.." Party Chat Disabled"
local LH_WHISPER_ENABLED_MSG = FULL_NAME.." Relief Healer Whisper Enabled"
local LH_WHISPER_DISABLED_MSG = FULL_NAME.." Relief Healer Whisper Disabled"
local LH_INITIALIZED_MSG = FULL_NAME.." Initialized"

-- ->Behavior Config
local whisperTargets = {}
local featurePartyEnabled = false
local featureWhisperEnabled = false
local actionableSpellNames = {}

-- ->Set up actionable spell list
actionableSpellNames["Lesser Heal"] = 1.5
actionableSpellNames["Heal"] = 3
actionableSpellNames["Flash Heal"] = 1.5
actionableSpellNames["Greater Heal"] = 3
actionableSpellNames["Healing Touch"] = 3.5
actionableSpellNames["Regrowth"] = 2
actionableSpellNames["Lesser Healing Wave"] = 1.5
actionableSpellNames["Healing Wave"] = 3
actionableSpellNames["Chain Heal"] = 3

-- ->Frame and event registration
local LazyHealerFrame = CreateFrame("Frame") -- Root frame
LazyHealerFrame:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
LazyHealerFrame:RegisterEvent("UNIT_SPELLCAST_START")
LazyHealerFrame:SetScript("OnEvent", function(self, event, ...) LazyHealerFrame:onEvent(self, event, ...) end);


-- <<------------------------------------------- APPLICATION ------------------------------------------->>


--Message handler for commands
SlashCmdList[SHORT_NAME] = function(msg)
	local _, _, cmd, args = string.find(msg, "%s?(%w+)%s?(.*)")
	if (not cmd or cmd == "" or cmd == "help") then
		print(FULL_NAME.." help:")
		print("General Commands:")
		print("  -" .. SLASH_LH1 .. " "..ARG_ENABLE.."                      -  Enables a feature: [" .. tableToString(FEATURE_TABLE) .. "]") 
		print("  -" .. SLASH_LH1 .. " "..ARG_DISABLE.."                     -  Disables a feature: [" .. tableToString(FEATURE_TABLE) .. "]") 
		print("  -" .. SLASH_LH1 .. " "..ARG_STATUS.."                       -  Show addon status") 
		print("ReliefPlayer Commands:")
		print("  -" .. SLASH_LH1 .. " "..ARG_ADD.."      -  Adds a relief player (Args: 1 player name)")
		print("  -" .. SLASH_LH1 .. " "..ARG_REMOVE.." -  Removes a relief player (Args: 1 player name)")
		print("  -" .. SLASH_LH1 .. " "..ARG_SHOW.."  -  All relief players to be messaged")
	elseif (cmd == ARG_ENABLE) then
		--print(ARG_ENABLE)
		enableFeature(args)
	elseif (cmd == ARG_DISABLE) then
		--print(ARG_DISABLE)
		disableFeature(args)
	elseif (cmd == ARG_ADD) then
		--print(ARG_ADD)
		addReliefPlayer(args)
	elseif (cmd == ARG_REMOVE) then
		--print(ARG_REMOVE)
		removeReliefPlayer(args)
	elseif (cmd == ARG_SHOW) then
		--print(ARG_SHOW)
		showReliefPlayers()
	elseif (cmd == ARG_STATUS) then
		--print(ARG_STATUS)
		reportStatus()
	else 
		print(FULL_NAME.." Argument not valid '" .. msg .. "' try " .. SLASH_LH1 .. " help")
	end
end 

--Enable a feature by name
--Return: Boolean
function enableFeature(arg)
	if arg == ARG_FEATURE_PARTY then
		featurePartyEnabled = true
	elseif arg == ARG_FEATURE_WHISPER then
		featureWhisperEnabled = true
	elseif arg == ARG_FEATURE_ALL then
		featurePartyEnabled = true
		featureWhisperEnabled = true
	end
	reportStatus()
end

--Disable a feature by name
--Return: Boolean
function disableFeature(arg)
	if arg == ARG_FEATURE_PARTY then
		featurePartyEnabled = false
	elseif arg == ARG_FEATURE_WHISPER then
		featureWhisperEnabled = false
	elseif arg == ARG_FEATURE_ALL then
		featurePartyEnabled = false
		featureWhisperEnabled = false
	end
	reportStatus()
end

--Prints the current application status
--Return: Void
function reportStatus()
	if(featurePartyEnabled==true) then
		print("->".. SHORT_NAME .. " [" .. ARG_FEATURE_PARTY .. "]        Enabled. To disable: '" .. ARG_CMD_DISABLE_FEATURE_PARTY .. "'")
	else 
		print("->".. SHORT_NAME .. " [" .. ARG_FEATURE_PARTY .. "]        Disabled. To enable: '" .. ARG_CMD_ENABLE_FEATURE_PARTY .. "'")
	end
	if(featureWhisperEnabled==true) then
		print("->".. SHORT_NAME .. " [" .. ARG_FEATURE_WHISPER .. "] Enabled. To disable: '" .. ARG_CMD_DISABLE_FEATURE_RELIEF_HEALER .. "'")
	else 
		print("->".. SHORT_NAME .. " [" .. ARG_FEATURE_WHISPER .. "] Disabled. To enable: '" .. ARG_CMD_ENABLE_FEATURE_RELIEF_HEALER .. "'")
	end
	print("Type " .. SLASH_LH1 .. " help for more info.")
end

--Removes a reliefPlayer by namne
--Return: Void
function removeReliefPlayer(nameArg)
	print(FULL_NAME .. " removing relief player: " .. nameArg)
	local index
	for i, name in ipairs(whisperTargets) do
		if nameArg==name then
			index = i
			break
		end
	end
	print("index: " .. index)
	table.remove(whisperTargets, index)
end

--Adds a reliefPlayer by name
--Return: Void
function addReliefPlayer(nameArg)
	print(FULL_NAME .. " adding relief player: " .. nameArg)
	local reliefPlayerAlreadyExists = false
	for i, name in ipairs(whisperTargets) do
		if nameArg==name then
			reliefPlayerAlreadyExists = true
			break
		end
	end
	
	if(reliefPlayerAlreadyExists == false) then
		table.insert(whisperTargets, nameArg)
	end
	
end

--Prints all reliefPlayerNames
--Return: Void
function showReliefPlayers()
	for i, name in ipairs(whisperTargets) do
		print (i .. ": " .. name)
	end
end

--UTIL: Converts a table (single dimension arrays only) to string of comma delimited values
--Return: String
function tableToString(table)
	local output = ""
	local valueCount = 0
	for i, value in ipairs(table) do
		if(valueCount == 0) then
			output = output .. value
		else
			output = output .. ", " .. value
		end
		valueCount = valueCount + 1
	end
	return output
end

--Returns a boolean conveys whether or not the given feature is active
--Return: Boolean
function isFeatureEnabled(featureName)
	if featureName == ARG_FEATURE_WHISPER then
		return featureWhisperEnabled
	elseif featureName == ARG_FEATURE_PARTY then
		return featurePartyEnabled
	end
	return false
end

--Broadcasts to the party that we're either almost out of mana or just outright OOM
--Return: Void
function communicateManaToParty(manaPercent)
	if isFeatureEnabled(ARG_FEATURE_PARTY) == true then
		if manaPercent <= 25 and manaPercent > 10 then
			SendChatMessage(format("Low on mana, %0.0f",manaPercent).."% remaining.", "PARTY")
		elseif manaPercent <= 10 then
			SendChatMessage(format("Out of mana (Effectively), %0.0f",manaPercent).."% remaining.", "PARTY")
		end
	end
end

--Whispers all known relief players that we're out of mana!
--Return: Void
function communicateManaToReliefPlayers(manaPercent)
	if isFeatureEnabled(ARG_FEATURE_WHISPER) == true then
		if manaPercent < 10 then
			for i, target in ipairs(whisperTargets) do
				SendChatMessage("I'm OOM, could use assistance if possible", "WHISPER", nil, target)
			end
		end
	end
end

--Broadcasts the healing target to the party
--Return: Void
function communicateHealingTargetToParty(castTime)
	if isFeatureEnabled(ARG_FEATURE_PARTY) == true then
		local name
		local realm
		if UnitIsEnemy("player","target") then
			name, realm = UnitName("player")
		else 
			name, realm = UnitName("target")
		end
		SendChatMessage("Healing " .. name .. " in " .. castTime .. " seconds", "PARTY")
	end
end
	
--Event handler for the LazyHealerFrame
function LazyHealerFrame:onEvent(self, event, ...)
	local unit, spell, rank = ...
	local spellId
	local spellName
	local castTime
	
	--I dont know how else to parse this.. these ID's are formatted like Cast3-0-444-2344-2-1234-32948E0 
	--wherein 1234 is the ID we can use with GetSpellInfo
	local count = 0
	for i in string.gmatch(spell, "%d+") do
		if count == 4 then 
			spellId = i
			break
		end 
		count = count + 1
	end
	
	spellName = GetSpellInfo(spellId)
	if actionableSpellNames[spellName] == nil then
		--Not a known healing spell.. nothing to communicate. End.
		return
	end
	
	if event == "UNIT_SPELLCAST_SUCCEEDED" and unit ~= "target" then
		manaPercent = (UnitPower("PLAYER")/UnitPowerMax("PLAYER"))*100
		communicateManaToParty(manaPercent)
		communicateManaToReliefPlayers(manaPercent)
	end
	if event == "UNIT_SPELLCAST_START" and unit ~= "target" then
		communicateHealingTargetToParty(actionableSpellNames[spellName])
	end
end

print(LH_INITIALIZED_MSG .. ". Current state ")
reportStatus()