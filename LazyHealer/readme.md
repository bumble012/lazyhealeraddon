## LazyHealer Addon for WoW classic

I've created this _mostly_ as an excersize to learn how to make WoW addons but it does actually provide a tiny bit of value should you be interested in using it.

At its core, its simply an addon that helps automate some of the healing tasks that I tend to find mundane or forgettable.. namely communicating mana status and giving other healers a heads-up as to what I'm doing.

Usage: In game just type `/lh help` to get started.

Changelog: 

- v1.0:
  - Adding feature 'partyChat': This feature communicates healing target and mana status
    - Communicates to the party when casting a heal (Currently only supporting basic priest heals, basic shaman heals, basic druid heals)
    - At >25% mana, will communicate to the party that we are 'Low on Mana'
    - At >10% will whisper to list of 'relief' or 'off' healers that we are 'Out of Mana' and that backup healing is needed (Currently this list defaults to 'Kabu', 'Blork', and 'Atruck' but this can be changed in game with the provided commands)
